package com.cognizant;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.boot.test.context.SpringBootTest;

import com.cognizant.controller.test.AuthControllerTests;
import com.cognizant.model.test.AuthorizationModelTest;
import com.cognizant.service.test.CustomUserDetailServiceTest;

//@RunWith(Suite.class)
//@SuiteClasses({ AuthControllerTests.class, AuthorizationModelTest.class, CustomUserDetailServiceTest.class })
@SpringBootTest
public class AuthorizationMicroserviceApplicationTests {

	@Test
	void contextLoads() {

	}
}
