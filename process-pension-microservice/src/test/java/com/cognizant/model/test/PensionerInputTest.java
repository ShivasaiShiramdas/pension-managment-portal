package com.cognizant.model.test;

import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.cognizant.model.BankDetail;
import com.cognizant.model.PensionDetail;
import com.cognizant.model.PensionerDetail;
import com.cognizant.model.PensionerInput;
import com.cognizant.model.ProcessPensionInput;
import com.cognizant.service.ProcessPensionService;

@SpringBootTest
public class PensionerInputTest {

	PensionerInput pensionerInput = new PensionerInput();

	@Test
	public void PensionerDetailBeanTest() {
		assertNotNull(pensionerInput);
	}

	@Test
	public void PensionerInputNoArgConstructorTest() {
		PensionerInput pensionerInput1 = new PensionerInput();
		assertThat(assertThat(pensionerInput1).isNotNull());
	}

	@Test
	public void PensionerInputAllArgConstructorTest() {
		PensionerInput pensionerInput1 = new PensionerInput("Pratyush", "06-11-1999", "PQWER12345", "1234567654678",
				"family");
		assertNotNull(pensionerInput1);
	}

	@Test
	public void PensionerDetailSettersTest() {
		PensionerInput pensionerDetail1 = new PensionerInput();
		pensionerDetail1.setAadhaarNumber("1211121324343543");
		pensionerDetail1.setName("Pratyush");
		pensionerDetail1.setDateOfBirth("06-11-1999");
		pensionerDetail1.setPanNumber("POQWERT12345");
		pensionerDetail1.setPensionType("family");
		assertEquals("1211121324343543", pensionerDetail1.getAadhaarNumber());
		assertEquals("Pratyush", pensionerDetail1.getName());
		assertEquals("06-11-1999", pensionerDetail1.getDateOfBirth());
		assertEquals("POQWERT12345", pensionerDetail1.getPanNumber());
		assertEquals("family", pensionerDetail1.getPensionType());

		assertNotNull(pensionerDetail1);

	}

	@Test
	public void ProcessPensionInputNoArgsTest() {
		ProcessPensionInput processPensionInput = new ProcessPensionInput();
		assertNotNull(processPensionInput);
	}

	@Test
	public void ProcessPensionInputAllArgsTest() {
		ProcessPensionInput processPensionInput = new ProcessPensionInput("123456789012", 32000.00, 500.00);
		assertNotNull(processPensionInput);
	}

	@Test
	public void ProcessPensionInputAllMethodsTest() {
		ProcessPensionInput processPensionInput = new ProcessPensionInput();
		processPensionInput.setAadhaarNumber("123456789012");
		processPensionInput.setBankCharge(32000.00);
		processPensionInput.setPensionAmount(500.00);
		assertEquals("123456789012", processPensionInput.getAadhaarNumber());
		assertEquals(500.00, processPensionInput.getPensionAmount());
		assertEquals(32000.00, processPensionInput.getBankCharge());
		assertNotNull(processPensionInput);
	}

	@Test
	public void ProcessPensionServiceAllArgsTest() throws NullPointerException {
		ProcessPensionService processPensionService = new ProcessPensionService();
		BankDetail bankDetail = new BankDetail("SBI","123456789012","Private");
		PensionerDetail pensionerDetails1 = new PensionerDetail("12904284925403", "Pratyush", "06-11-1999",
				"PQWER12345", 40000.00, 12000.00, "family", bankDetail);
		PensionerInput pensionerInput1 = new PensionerInput("Pratyush", "06-11-1999", "PQWER12345", "1234567654678",
				"family");
		PensionDetail pensionDetail = processPensionService.getPensionDetail(pensionerDetails1, pensionerInput1);
		assertNotNull(pensionDetail);
		// PensionDetail pensionDetails1 = new PensionDetail(1, "Pratyush",
		// "06-11-1999", "PQWER12345", "family",
		// 40000.00);
		// PensionDetail pensionDetail1 =
		// processPensionService.savePensionDetail(pensionDetails1);
		// assertNotNull(pensionDetail1);
	}
}
