package com.cognizant.model.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.cognizant.exception.ControllerAdviceClass;
import com.cognizant.exception.ErrorResponse;
import com.cognizant.exception.ProcessPensionException;
import com.cognizant.model.BankDetail;

@SpringBootTest
public class BankDetailModelTest {

	BankDetail bankDetail = new BankDetail();

	@Test
	public void BankDetailBeanTest() {
		assertNotNull(bankDetail);
	}

	@Test
	public void BankDetailNoArgConstructorTest() {
		BankDetail bankDetail1 = new BankDetail();
		assertThat(assertThat(bankDetail1).isNotNull());

	}

	@Test
	public void BankDetailAllArgConstructorTest() {
		BankDetail bankDetail1 = new BankDetail("SBI", "123456789009", "private");
		assertNotNull(bankDetail1);
	}

	@Test
	public void BankDetailSettersTest() {
		BankDetail bankDetail1 = new BankDetail();
		bankDetail1.setAccountNumber("1111111111");
		bankDetail1.setBankName("SBI");
		bankDetail1.setBankType("public");
		assertEquals("1111111111", bankDetail1.getAccountNumber());
		assertEquals("SBI", bankDetail1.getBankName());
		assertEquals("public", bankDetail1.getBankType());
		assertNotNull(bankDetail);
	}

	@Test
	public void ErrorResponseNoArgsTest() {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setExceptionTime(1000L);
		errorResponse.setMessage("Shiva");
		errorResponse.setStatusCode(50);
		assertEquals(String.valueOf(1000L), String.valueOf(errorResponse.getExceptionTime()));
		assertEquals("Shiva", errorResponse.getMessage());
		assertEquals(50, errorResponse.getStatusCode());
		assertNotNull(errorResponse);
	}

	@Test
	public void ErrorResponseAllArgsTest() {
		ErrorResponse errorResponse = new ErrorResponse("Shiva", 50, 1000L);
		ErrorResponse error = new ErrorResponse();
		Boolean b = errorResponse.equals(error);
		assertNotNull(b);
		String str = errorResponse.toString();
		assertNotNull(errorResponse.hashCode());
		assertNotNull(str);
		assertNotNull(errorResponse);
	}

	@Test
	public void ControllerAdviceClassTest() {
		ProcessPensionException processPensionException = new ProcessPensionException();
		ControllerAdviceClass controllerAdviceClass = new ControllerAdviceClass();
		ResponseEntity<ErrorResponse> error = controllerAdviceClass.controllerAdviceResponse(processPensionException);
		assertNotNull(error);
	}

	@Test
	public void ProcessPensionExceptionNoArgsTest() {
		ProcessPensionException processPensionException = new ProcessPensionException();
		processPensionException.setMessage("Shiva");
		assertEquals("Shiva", processPensionException.getMessage());
		assertNotNull(ProcessPensionException.getSerialversionuid());
		assertNotNull(processPensionException);
	}

	@Test
	public void ProcessPensionExceptionAllArgsTest() {
		ProcessPensionException processPensionException = new ProcessPensionException("Shiva");
		assertNotNull(processPensionException);
	}
}
