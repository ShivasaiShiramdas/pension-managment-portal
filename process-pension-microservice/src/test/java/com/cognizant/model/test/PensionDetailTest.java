package com.cognizant.model.test;

import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.cognizant.model.PensionDetail;
import com.cognizant.model.ProcessPensionResponse;

@SpringBootTest
public class PensionDetailTest {

	PensionDetail pensionDetails = new PensionDetail();

	@Test
	public void PensionDetailBeanTest() {
		assertNotNull(pensionDetails);
	}

	@Test
	public void PensionDetailNoArgConstructorTest() {
		PensionDetail pensionDetails1 = new PensionDetail();
		assertThat(assertThat(pensionDetails1).isNotNull());

	}

	@Test
	public void PensionDetailAllArgConstructorTest() {
		PensionDetail pensionDetails1 = new PensionDetail(1, "Pratyush", "06-11-1999", "PQWER12345", "family",
				40000.00);
		assertEquals("06-11-1999", pensionDetails1.getDateOfBirth());
		assertEquals("Pratyush", pensionDetails1.getName());
		assertEquals("PQWER12345", pensionDetails1.getPanNumber());
		assertEquals(40000.00, pensionDetails1.getPensionAmount());
		assertEquals("family", pensionDetails1.getPensiontype());
		assertEquals(1, pensionDetails1.getId());
		assertNotNull(pensionDetails1);
	}

	@Test
	public void PensionDetailAllArgConstructorTest1() {
		PensionDetail pensionDetails2 = new PensionDetail("Pratyush", "06-11-1999", "PQWER12345", "family", 40000.00);
		assertNotNull(pensionDetails2);
	}

	@Test
	public void PensionDetailAllArgConstructorTest2() {
		PensionDetail pensionDetails2 = new PensionDetail("Pratyush", "06-11-1999", "PQWER12345", "family", 40000.00,
				500);
		assertEquals(500, pensionDetails2.getBankServiceCharge());
		assertNotNull(pensionDetails2);
	}

	@Test
	public void PensionDetailSettersTest() {
		PensionDetail pensionDetail1 = new PensionDetail();
		pensionDetail1.setId(1);
		pensionDetail1.setName("Pratyush");
		pensionDetail1.setDateOfBirth("06-11-1999");
		pensionDetail1.setPanNumber("POQWERT12345");
		pensionDetail1.setPensiontype("family");
		pensionDetail1.setPensionAmount(40000.00);
		pensionDetail1.setBankServiceCharge(550);
		assertNotNull(pensionDetail1);

	}

	@Test
	public void ProcessPensionResponseAllArgTest() {
		ProcessPensionResponse processpensionResponse = new ProcessPensionResponse(100);
		assertEquals(100, processpensionResponse.getProcessPensionStatusCode());
		assertNotNull(processpensionResponse);
	}

	@Test
	public void ProcessPensionResponseNoArgTest() {
		ProcessPensionResponse processpensionResponse = new ProcessPensionResponse();
		processpensionResponse.setProcessPensionStatusCode(100);
		assertNotNull(processpensionResponse);
	}
}