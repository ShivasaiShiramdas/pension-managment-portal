package com.cognizant.model;

public class AuthRequest {

	private String userName;
	private String password;

	public AuthRequest(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	public AuthRequest() {
		// TODO Auto-generated constructor stub
	}

	public void setPassword(String password) {
		this.password = password;

	}

	public void setUserName(String userName) {
		this.userName = userName;

	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

}
