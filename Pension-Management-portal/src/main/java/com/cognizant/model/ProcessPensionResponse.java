package com.cognizant.model;

public class ProcessPensionResponse {

	private int processPensionStatusCode;

	public ProcessPensionResponse(int processPensionStatusCode) {
		super();
		this.processPensionStatusCode = processPensionStatusCode;
	}

	public ProcessPensionResponse() {
		// TODO Auto-generated constructor stub
	}

	public int getProcessPensionStatusCode() {
		return processPensionStatusCode;
	}

	public void setProcessPensionStatusCode(int processPensionStatusCode) {
		this.processPensionStatusCode = processPensionStatusCode;
	}

}
