package com.cognizant.controller.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.cognizant.controller.PensionController;

@SpringBootTest
public class PensionManagementControllerTest {

	@Autowired
	PensionController pensionController;

	@Test
	public void PensionControllerTest() {
		String s = pensionController.display();
		assertNotNull(s);
	}

}
